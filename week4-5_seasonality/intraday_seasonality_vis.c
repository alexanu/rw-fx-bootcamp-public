#include <profile.c>

function run()
{
	set(PLOTNOW);
  	StartDate = 2008;
  	EndDate = 2018;
  	BarPeriod = 60;
 
  	vars Close = series(priceClose());
  	vars Return = series((Close[0] - Close[1])/Close[1]);

  	plotDay(Return[0], 0);
}