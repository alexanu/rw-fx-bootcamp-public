#include <profile.c>

function run()
{
	set(PLOTNOW);
  	StartDate = 2010;
  	EndDate = 2018;
  	BarPeriod = 60;
  	LookBack = 120;
  	Weekend = 1;
 
  	vars Close = series(priceClose());
  	vars Return = series((Close[0] - Close[1])/Close[1]);
  	
  	plotWeek(Return[0], 1);
}